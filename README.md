##### Summary
This app provides dashboards for your health, running and cycling data.

##### Requirements
This app requires the following add-ons to be installed:

- [Strava for Splunk](https://splunkbase.splunk.com/app/4755/) (for activity data)
- [Garmin Add-On for Splunk](https://splunkbase.splunk.com/app/5035) (for heart rate, sleep and oxygen saturation data)
- [Fitbit TA for Splunk](https://splunkbase.splunk.com/app/5066) (for heart rate and body scale data)
- [Stryd Add-On for Splunk](https://splunkbase.splunk.com/app/5066) (for running power data)

##### Usage
- Update the macro `fitness_indexes` to reflect the correct indexes for your data sources.

##### Acknowledgements
- Icon made by [Freepik](https://www.freepik.com) from [Flaticon](https://www.flaticon.com/).